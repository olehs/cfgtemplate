import { createContext } from "react";

export const ConfigContext = createContext({
  language: "",
  getTranslation: (text, language) => {},
  getConfigValue: async (property, path) => {},
  setConfigValue: async (property, path, value) => {},
  getProperty: (property, path) => {},
});
