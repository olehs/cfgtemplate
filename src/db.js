import Dexie from "dexie";

export const db = new Dexie("termDatabase");

const initDb = (transaction) => {
  transaction.terminal_merchants.clear();
  transaction.terminals.clear();

  transaction.terminals.add({
    id: 1,
    uid: "00000001",
    bank_id: 1,
  });
  transaction.terminal_merchants.add({
    terminal_id: 1,
    uid: "M999103",
  });
  transaction.terminal_merchants.add({
    terminal_id: 1,
    uid: "M999104",
  });
};

db.version(1).stores({
  terminals: "&id, terminalId, terminalName",
  merchants: "++id, terminalId, merchantId, merchantName",
});

db.version(2).stores({
  terminals: "&id, uid",
  merchants: "++id, terminal_id, uid",
});

db.version(3).stores({
  terminals: "&id, uid",
  terminal_merchants: "++id, terminal_id, uid",
});

db.version(4).stores({
  terminals: "&id, uid, bank_id",
  terminal_merchants: "++id, terminal_id, uid",
})
  .upgrade(initDb);

db.on("populate", initDb);

db.open();
