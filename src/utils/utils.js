export function parsePath(path, arrayKey) {
  let result = [];

  if (typeof path !== "string") return [path];

  const parts = path.split("/");

  for (let i = 0; i < parts.length; i++) {
    let item = parts[i];
    if (item === "" || item === ".") {
      if (i === 0) item = "#";
      else continue;
    } else if (item === "..") {
      if (result.length > 1) {
        result.pop();
      }
      continue;
    } else {
      const array = item.match(/^(.+)\[(\d+)\]$/);
      if (array) {
        result.push(arrayKey ? [array[1]] : array[1]);
        item = parseInt(array[2]);
      }
    }

    if (item === "#") {
      result = [item];
    } else {
      result.push(item);
    }
  }
  return result;
}

export function createValue(type) {
  switch (type) {
    case undefined:
    case "object":
      return {};

    // case "string":
    //   return "";

    // case "integer":
    //   return 0;

    // case "boolean":
    //   return false;

    case "array":
      return [];

    default:
      break;
  }
}

export const isObject = (item) => {
  return (item && typeof item === 'object' && !Array.isArray(item));
}

export const mergeValue = (target, ...sources) => {
  if (!sources.length) return target;
  const source = sources.shift();

  if (isObject(target) && isObject(source)) {
    for (const key in source) {
      if (isObject(source[key])) {
        if (!target[key]) Object.assign(target, {
          [key]: {}
        });
        mergeValue(target[key], source[key]);
      } else {
        Object.assign(target, {
          [key]: source[key]
        });
      }
    }
  }

  return mergeValue(target, ...sources);
}

export function hashCode(obj) {
  const str = JSON.stringify(obj);
  let hash = 0,
    i,
    chr;
  if (str.length === 0) return hash;
  for (i = 0; i < str.length; i++) {
    chr = str.charCodeAt(i);
    hash = (hash << 5) - hash + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash;
}
