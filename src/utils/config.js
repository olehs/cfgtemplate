import jp from "jsonpath";
import { db } from "../db";
import { createValue, mergeValue, parsePath } from "./utils";
import { schemaAtPath } from "./schema";

const dbTerminalId = 1;

export async function loadConfig(schema, config) {
  let changed = false;

  if (!config) {
    config = {};
    changed = true;
  }

  if (config.id !== dbTerminalId) {
    config.id = dbTerminalId;
    changed = true;
  }

  if (schema?.schema) {
    changed = await prepareConfig(schema, config, schema.schema, "");
  }
  return changed ? { ...config } : config;
}

const toEnum = (element, key) => (element instanceof Object ? element : { [key]: element, name: element });

async function prepareConfig(schema, config, node, path, key) {
  const pathName = node.path || key;
  const propertyPath = `${path}${pathName ? "/" + pathName : ""}`;

  let changed = false;

  if (node.type === "group") {
    for (let i = 0; i < node.items.length; i++) {
      const item = node.items[i];
      if (await prepareConfig(schema, config, item, propertyPath)) changed = true;
    }
    return changed;
  }

  let [value, modified] = await getConfigValue(schema, config, node, propertyPath);
  if (modified) changed = true;

  switch (node.type) {
    case undefined:
    case "object":
      if (node.enum) {
        const keyProp = node.key ?? "id";
        const isMulti = node.multiselect;
        const items = node.enum.map(e => toEnum(e, keyProp));

        const currValue = isMulti ? value?.map(e => toEnum(e, keyProp)) : toEnum(value, keyProp);

        const findItem = (value) => {
          if (isMulti) {
            return items.filter((item) => value?.some((v) => item[keyProp] === v?.[keyProp]));
          }
          return items.find((item) => item[keyProp] === value?.[keyProp]);
        };

        const item = findItem(currValue);
        if (item) {
          if (await prepareConfig(schema, config, item, propertyPath)) changed = true;
        }

        return changed;
      }

      for (const propKey of Object.keys(node.properties)) {
        if (await prepareConfig(schema, config, node.properties[propKey], propertyPath, propKey)) changed = true;
      }
      break;

    case "array":
      node.items.required = true
      if (value) {
        for (let i = 0; i < value.length; i++) {
          if (await prepareConfig(schema, config, node.items, `${propertyPath}[${i}]`)) changed = true;
        }
      }
      break;

    default:
      break;
  }
  return changed;
}

async function getConfigValue(schema, config, prop_node, path) {
  const parts = parsePath(path);
  if (!parts.length) return [];

  let node = config;
  let needsUpdate = false;
  for (let i = 0; i < parts.length; i++) {
    let key = parts[i];

    if (key === "#") {
      node = config;
      continue;
    }

    if (node === undefined) return []

    if (node instanceof Object && node[key] === undefined) {
      if (i < parts.length - 1) return [];

      if (prop_node.default !== undefined) {
        node[key] = await resolveQuery(schema, config, prop_node, prop_node.default);
      } else if (prop_node.required) {
        node[key] = createValue(prop_node.type);
      }
      if (node[key] !== undefined) needsUpdate = true;
    }

    if (i === parts.length - 1) {
      const [newValue, changed] = await resolveValue(schema, config, prop_node, node[key]);
      if (changed) {
        node[key] = newValue;
        needsUpdate = true;
      }
    }

    node = node[key];
  }
  return [node, needsUpdate];
}

export async function resolveValue(schema, config, prop_node, value) {
  if (prop_node.value === undefined) return [value, false];

  const newValue = await resolveQuery(schema, config, prop_node, prop_node.value);
  if (value === undefined) return [newValue, value !== newValue];

  switch (prop_node.type) {
    case "array":
      if (prop_node.items.type === undefined || prop_node.items.type === "object") {
        let changed = false;
        const key = prop_node.key ?? "id";

        value.forEach((dst, i) => {
          const src = newValue.find((v) => v[key] === dst[key]);
          if (src) {
            if (mergeValue(dst, src)) changed = true;
          } else {
            value.splice(i, 1);
            changed = true;
          }
        });

        newValue.forEach((src, i) => {
          const dst = value.find((v) => v[key] === src[key]);
          if (!dst) {
            value.splice(i, 0, src);
            changed = true;
          }
        });
        return [value, changed];
      } else return [newValue, true];

    case undefined:
    case "object":
      return [value, mergeValue(value, newValue)];

    default:
      return [newValue, value !== newValue];
  }
}

async function resolveQuery(schema, config, property, value) {
  if (typeof value !== "string") return value;

  const jpath = value.match(/^\$query:(.+?)(\.(.+))?$/);
  if (!jpath) return value;

  const data = await getQueryValue(schema, config, jpath[1], jpath[3]);
  return property.type === "array" ? data : data[0];
}

async function getQueryValue(schema, config, name, path) {
  const query = schemaAtPath(schema, "$.query." + name);
  if (!query) return;

  let request = db[query.table];
  if (!request) return

  const where = query.where || {};
  Object.keys(where).forEach((key) => {
    const values = configAtPath(config, where[key]);
    request = request.where(key).anyOf(...values);
  });

  let data = await request.toArray();
  if (query.projection) {
    const keys = Object.keys(query.projection);
    data = data.map((item) => {
      let result = {};
      keys.forEach((key) => {
        result[key] = item[query.projection[key]];
      });
      return result;
    });
  }

  return path ? data.map((el) => jp.value(el, path)) : data;
}

function configAtPath(config, path) {
  if (typeof path === "string" && path.startsWith("$path:")) {
    return jp.query(config, path.slice(6));
  }
  return [path];
}
