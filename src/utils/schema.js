import yaml from "js-yaml";
import jp from "jsonpath";
import { mergeValue, parsePath } from "./utils";

export function loadSchema(schemaStr, language, trimHidden) {
  if (!schemaStr) return {};

  try {
    const schemas = yaml.loadAll(schemaStr);
    const schema = mergeSchemas(schemas);
    schema.schema = resolveProperty(schema, schema.schema, language, trimHidden);
    return schema;
  } catch (e) {
    console.error(e);
    // throw e;
  }
}

function resolveTransaltions(schema, property, language) {
  if (property.name !== undefined) {
    property.name = resolveRef(schema, property.name);
  }

  if (property.description !== undefined) {
    property.description = resolveRef(schema, property.description);
  }

  if (language) {
    if (property.name != null) {
      property.name = getTranslation(schema, property.name || "", language);
    }
    if (property.description != null) {
      property.description = getTranslation(schema, property.description || "", language);
    }
  }

  return property;
}

export function resolveProperty(schema, property, language, trimHidden) {
  property = resolveRef(schema, property);

  if (property.$allOf !== undefined) {
    property.$allOf = resolveRef(schema, property.$allOf);

    if (Array.isArray(property.$allOf)) {
      for (let item of property.$allOf) {
        item = resolveRef(schema, item);
        if (item !== undefined) {
          mergeValue(property, item);
        }
      }

      delete property.$allOf;
    }
  }

  if (trimHidden === true && property.hidden) {
    return;
  }

  if (property.default !== undefined) {
    property.default = resolveRef(schema, property.default);
  }

  if (property.value !== undefined) {
    property.value = resolveRef(schema, property.value);
  }

  resolveTransaltions(schema, property, language);

  if (property.type === "group") {
    property.items = property.items
      .map((item) => resolveProperty(schema, item, language, trimHidden))
      .filter((item) => item !== undefined);
    return property;
  }

  if (property.enum) {
    property.enum = resolveRef(schema, property.enum) || [];
    property.enum = property.enum
      .map((item) => {
        if (property.type === undefined || property.type === "object") {
          return resolveProperty(schema, item, language, trimHidden);
        } else {
          if (item instanceof Object) {
            item = resolveRef(schema, item);
            resolveTransaltions(schema, item, language);
          }
          return item;
        }
      })
      .filter((item) => item !== undefined);
    return property;
  }

  switch (property.type) {
    case undefined:
    case "object":
      if (property.properties !== undefined) {
        property.properties = resolveRef(schema, property.properties);
      }

      for (const key of Object.keys(property.properties)) {
        const prop = resolveProperty(schema, property.properties[key], language, trimHidden);
        if (prop !== undefined) {
          property.properties[key] = prop;
        } else {
          delete property.properties[key];
        }
      }

      break;

    case "array":
      property.items = resolveProperty(schema, property.items, language, trimHidden);
      break;

    default:
      break;
  }

  return property;
}

function resolveRef(schema, obj) {
  while (obj && typeof obj === "object" && obj.$ref != null) {
    const ref = structuredClone(schemaAtPath(schema, obj.$ref));
    delete obj.$ref;
    mergeValue(ref, obj);
    obj = ref;
  }
  return obj;
}

export function schemaAtPath(schema, path) {
  return jp.value(schema, jPath(path));
}

export function jPath(path) {
  let p = path.startsWith("#") ? "$.." + path : path;
  p = p.replace(/(\(.+?)=(.+?\))/g, "$1==$2"); // python -> javascript syntax conversion
  p = p.replace(".*[?(", "[?("); // python -> javascript syntax conversion
  p = p.replace(/(#(.+?)(\.|$))/g, "[?(@['$id']=='$2')]$3");
  return p;
}

export function mergeSchemas(schemas) {
  const obj = schemas.reduce((p, c) => {
    const schema = { ...p, ...c };
    patchSchema(schema);
    return schema;
  }, {});

  return obj;
}

function patchSchema(schema) {
  if (!schema.patch) return;

  const patches = Array.isArray(schema.patch) ? schema.patch : [schema.patch];

  patches.forEach((patch) => processPatch(schema, patch));
  delete schema.patch;
}

function processPatch(schema, patch) {
  if (Array.isArray(patch)) {
    patch.forEach((p) => processPatch(schema, p));
    return;
  }

  let root = patch.root ? jPath(patch.root) : null;
  let nodes;
  if (!patch.path) {
    if (!root) return;
    nodes = jp.paths(schema, root).map((elements) => {
      const p = elements.pop();
      const r = jp.stringify(elements);
      return [jp.value(schema, r), p];
    });
  } else {
    nodes = root ? jp.query(schema, root).map((e) => [e, patch.path]) : [schema, patch.path];
  }

  if (!nodes?.length) return;

  nodes.forEach(([n, p]) => {
    switch (patch.op) {
      case "add": {
        const [node, key] = walkPath(schema, n, p, true);
        const currNode = node[key];
        if (Array.isArray(currNode)) {
          if (patch.before === undefined) {
            currNode.push(patch.value);
          } else {
            currNode.splice(patch.before, 0, patch.value);
          }
        } else if (node instanceof Object) {
          if (node.hasOwnProperty(key)) {
            mergeValue(currNode, patch.value);
          } else {
            node[key] = patch.value;
          }
        }
        break;
      }

      case "replace": {
        const [node, key] = walkPath(schema, n, p, true);
        node[key] = patch.value;
        break;
      }

      case "remove": {
        const [node, key] = walkPath(schema, n, p);
        if (Array.isArray(node)) {
          node.splice(key, 1);
        } else {
          delete node[key];
        }
        break;
      }

      default:
        break;
    }
  });
}

function walkPath(schema, node, path, forceCreate) {
  const paths = parsePath(path, true);

  let key = null;
  for (let i = 0; i < paths.length; i++) {
    key = paths[i];

    if (key === "#") {
      node = schema;
      key = null;
      continue;
    }

    if (Array.isArray(key)) {
      key = key[0];
      if (!Array.isArray(node[key])) {
        if (forceCreate) node[key] = [];
        else return;
      }
    }

    if (i < paths.length - 1) {
      if (!node.hasOwnProperty(key)) {
        if (forceCreate) node[key] = {};
        else return;
      }
      node = node[key];
    }
  }

  return [node, key];
}

export function getTranslation(schema, text, language) {
  if (!text.startsWith("$tr:")) return text;

  let id = text.slice(4);
  const tr = schema.tr?.[language]?.[id] ?? schema.tr?.default?.[id];
  return tr ?? id;
}
