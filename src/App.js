import "./App.css";
import "react-widgets/styles.css";
import "react-tabs/style/react-tabs.css";
import { useEffect, useState } from "react";
import { Settings } from "./components/Settings";
import { Config } from "./components/Config";
import { ConfigContext } from "./ConfigContext";
import { Schema } from "./components/Schema";
import { getTranslation, loadSchema } from "./utils/schema";
import { parsePath } from "./utils/utils";
import { loadConfig, resolveValue } from "./utils/config";

function App() {
  const [config, setConfig] = useState(() => {
    const data = sessionStorage.getItem("config");
    if (data) return JSON.parse(data);
  });

  const [schema, setSchema] = useState(sessionStorage.getItem("schema"));
  const [schemaObj, setSchemaObj] = useState({});
  const [schemaTrimmed, setSchemaTrimmed] = useState({});
  const [lang] = useState("ua");

  useEffect(() => {
    try {
      const obj = loadSchema(schema, lang);
      if (obj) {
        setSchemaObj(obj);
      }
      const trim = loadSchema(schema, lang, true);
      if (trim) {
        setSchemaTrimmed(trim);
      }
    } catch (e) {
      console.error(e);
    }
  }, [schema, lang]);

  useEffect(() => {
    if (!schema) {
      fetch("/config.yml")
        .then((response) => response.text())
        .catch(() => null)
        .then((yml) => {
          setSchema(yml || "");
        });
    }
  }, [schema]);

  useEffect(() => {
    sessionStorage.setItem("schema", schema);
  }, [schema]);

  useEffect(() => {
    if (config) {
      sessionStorage.setItem("config", JSON.stringify(config));
    }
  }, [config]);

  useEffect(() => {
    loadConfig(schemaObj, config).then((cfg) => {
      setConfig(cfg);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [schemaObj]);

  async function getConfigValue(property, path) {
    const parts = parsePath(path);
    if (!parts.length) return;

    let node = config;
    for (let i = 0; i < parts.length; i++) {
      let key = parts[i];

      if (key === "#") {
        node = config;
        continue;
      }
      if (!node?.hasOwnProperty(key)) return;
      node = node[key];
    }

    return node;
  }

  async function setConfigValue(property, path, value) {
    const parts = parsePath(path);
    if (!parts.length) return;

    let obj = config;
    for (let i = 0; i < parts.length; i++) {
      let key = parts[i];

      if (key === "#") {
        obj = config;
        continue;
      }

      if (i === parts.length - 1) {
        const [newValue] = await resolveValue(schemaObj, config, property, value);
        obj[key] = newValue;
      } else {
        if (!obj[key]) {
          obj[key] = {};
        }
        obj = obj[key];
      }
    }

    await loadConfig(schemaObj, { ...config }).then((cfg) => {
      setConfig(cfg);
    });
  }

  function getProperty(property) {
    return property?.value !== undefined ? { ...property, readonly: true } : property;
  }

  return (
    <div className="App">
      <ConfigContext.Provider
        value={{
          language: lang,
          getTranslation: (text, language) => getTranslation(schemaObj, text, language ?? lang),
          getConfigValue,
          setConfigValue,
          getProperty,
        }}
      >
        {schema ? <Schema schema={schema} setSchema={setSchema} /> : null}
        {schemaTrimmed && config ? <Settings schemaObj={schemaTrimmed.schema} /> : ""}
        {config ? (
          <Config
            config={config}
            setConfig={(cfg) =>
              loadConfig(schemaObj, cfg).then((cfg) => {
                setConfig(cfg);
              })
            }
          />
        ) : null}
      </ConfigContext.Provider>
    </div>
  );
}

export default App;
