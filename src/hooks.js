import { useContext, useEffect, useState } from "react";
import { ConfigContext } from "./ConfigContext";

export function useConfig(property, path) {
  const config = useContext(ConfigContext);
  const [value, setValue] = useState();

  useEffect(() => {
    config.getConfigValue(property, path).then(setValue);
  }, [config, path, property]);

  return [value, async (newValue) => await config.setConfigValue(property, path, newValue)];
}

export function useProperty() {
  const config = useContext(ConfigContext);
  return (property) => config.getProperty(property);
}

export function useTranslation() {
  const config = useContext(ConfigContext);
  return [config.getTranslation, config.language];
}
