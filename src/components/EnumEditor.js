import { Multiselect } from "react-widgets";
import { DropdownList } from "react-widgets";
import { useProperty, useConfig, useTranslation } from "../hooks";
import { Editor } from "./Editor";

const toEnum = (element) => (element instanceof Object ? element : { id: element, name: element });

export function EnumEditor({ property, path }) {
  const getProperty = useProperty();
  const [value, setValue] = useConfig(property, path);
  const [getTranslation] = useTranslation();

  const items = getProperty(property.enum)
    .map((element) => getProperty(element))
    .map(toEnum);

  const keyProp = property.key ?? "id";
  const isObject = property.type === undefined || property.type === "object";
  const isMulti = property.multiselect;

  const currValue = isMulti ? value?.map(toEnum) : toEnum(value);

  const findItem = (value) => {
    if (isMulti) {
      return items.filter((item) => value?.some((v) => item.id === v?.[keyProp]));
    }

    return items.find((item) => item.id === value?.[keyProp]);
  };

  const defaultValue = findItem(currValue);

  const valueChanged = (newValue) => {
    setValue(
      isObject
        ? isMulti
          ? newValue
          : { ...currValue, [keyProp]: newValue.id }
        : isMulti
          ? newValue.map((v) => v.id)
          : newValue.id
    );
  };

  const listProps = {
    className: "EnumEditor",
    value: defaultValue,
    dataKey: keyProp,
    textField: (item) => getTranslation(item.name),
    data: items,
    readOnly: !!property.readonly,
    onChange: valueChanged,
  };

  return (
    <div
      className={
        "ObjectEditor" +
        (property.required && (!defaultValue || (isMulti && !defaultValue.length)) ? " RequiredTitle" : "")
      }
    >
      {isMulti ? <Multiselect {...listProps} /> : <DropdownList {...listProps} />}
      {defaultValue && !isMulti ? (
        <div className="Editor">
          <Editor keyValue="" property={defaultValue} path={path} />
        </div>
      ) : null}
    </div>
  );
}
