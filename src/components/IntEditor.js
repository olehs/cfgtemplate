import { NumberPicker } from "react-widgets";
import { useConfig } from "../hooks";

export function IntEditor({ property, path }) {
  const [value, setValue] = useConfig(property, path);

  return (
    <NumberPicker
      className={"IntEditor" + (property.required && value == null ? " RequiredTitle" : "")}
      readOnly={!!property.readonly}
      value={value}
      onChange={(value) => {
        setValue(value);
      }}
    />
  );
}
