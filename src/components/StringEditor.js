import { useConfig } from "../hooks";

export function StringEditor({ property, path }) {
  const [value, setValue] = useConfig(property, path);

  return (
    <input
      className={"StringEditor" + (property.required && (value == null || value === "") ? " RequiredTitle" : "")}
      type="text"
      readOnly={!!property.readonly}
      defaultValue={value}
      onChange={(event) => {
        setValue(event.target.value);
      }}
    />
  );
}
