import { Editor } from "./Editor";

export function Settings({ schemaObj }) {
  return (
    <div className="Settings">
      <Editor className="Editor" keyValue="" property={schemaObj} path="" />
    </div>
  );
}
