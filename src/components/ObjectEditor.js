import { useConfig, useProperty, useTranslation } from "../hooks";
import { Editor } from "./Editor";

export function ObjectEditor(props) {
  const [value] = useConfig(props.property, props.path);
  const getProperty = useProperty();
  const [getTranslation] = useTranslation();

  const properties = props.property.properties;
  if (!properties) return null;
  const keys = Object.keys(properties);

  return (
    <div className={"ObjectEditor" + (props.property.required && !value ? " RequiredTitle" : "")}>
      {keys.length > 0 &&
        keys.map((keyValue) => {
          let property = getProperty(properties[keyValue]);
          if (property.visible === false) return null;
          return (
            <div key={keyValue} className="Editor">
              {property.name && <span>{getTranslation(property.name)}:</span>}
              <Editor keyValue={keyValue} property={property} path={props.path} />
            </div>
          );
        })}
    </div>
  );
}
