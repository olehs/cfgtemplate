import { DropdownList } from "react-widgets";
import { useConfig } from "../hooks";

export function BooleanEditor({ property, path }) {
  const [value, setValue] = useConfig(property, path);

  return (
    <DropdownList
      className={"BooleanEditor" + (property.required && value == null ? " RequiredTitle" : "")}
      value={value === true ? "True" : value === false ? "False" : undefined}
      data={["True", "False"]}
      readOnly={!!property.readonly}
      onChange={(value) => {
        switch (value) {
          case "True":
            setValue(true);
            break;
          case "False":
            setValue(false);
            break;
          default:
            setValue(null);
            break;
        }
      }}
    />
  );
}
