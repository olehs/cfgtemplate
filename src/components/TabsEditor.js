import { Tab, TabList, TabPanel, Tabs } from "react-tabs";
import { useTranslation } from "../hooks";
import { Editor } from "./Editor";

export function TabsEditor({ property, path }) {
  const [getTranslation] = useTranslation();
  const items = property.items || [];

  return (
    <Tabs className={"TabsEditor"}>
      <TabList>
        {items.map((item, i) => (
          <Tab key={i}>{getTranslation(item.name)}</Tab>
        ))}
      </TabList>
      {items.map((item, i) => (
        <TabPanel key={i}>
          <Editor keyValue="" property={item} path={path} />
        </TabPanel>
      ))}
    </Tabs>
  );
}
