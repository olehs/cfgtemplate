import { useState } from "react";
import MonacoEditor from "react-monaco-editor";
import yaml from "js-yaml";
import { loadSchema } from "../utils/schema";
import { useTranslation } from "../hooks";

function trimSchema(schema, language) {
  const sch = loadSchema(schema, language, true);
  return { schema: sch.schema, query: sch.query, tr: language ? undefined : sch.tr };
}

export function Schema({ schema, setSchema }) {
  const [mode, setMode] = useState("source");
  let language; // = useTranslation()[1];

  switch (mode) {
    case "yaml":
      var sch = trimSchema(schema, language);
      sch = yaml.dump(sch);
      break;

    case "json":
      sch = trimSchema(schema, language);
      sch = JSON.stringify(sch, null, 2);
      break;

    default:
      sch = schema;
      break;
  }
  return (
    <div className="Schema">
      <div>
        <button className={mode === "source" ? "pushed" : ""} onClick={() => setMode("source")}>
          Source
        </button>
        <button className={mode === "yaml" ? "pushed" : ""} onClick={() => setMode("yaml")}>
          PreProc (YAML)
        </button>
        <button className={mode === "json" ? "pushed" : ""} onClick={() => setMode("json")}>
          PreProc (JSON)
        </button>
        <button onClick={() => setSchema("")}>RESET</button>
      </div>
      <MonacoEditor
        language={mode === "json" ? "json" : "yaml"}
        value={sch || ""}
        onChange={(value) => setSchema(value)}
        options={{
          lineNumbers: "off",
          automaticLayout: true,
          readOnly: mode !== "source",
          fontSize: "12"
        }}
      />
    </div>
  );
}
