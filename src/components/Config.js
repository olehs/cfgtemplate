import MonacoEditor from "react-monaco-editor";

export function Config({ config, setConfig }) {
  return (
    <div className="Config">
      <div>
        <button onClick={() => setConfig({})}>RESET</button>
      </div>

      <MonacoEditor
        language="json"
        value={config ? JSON.stringify(config, null, 2) : ""}
        options={{
          readOnly: true,
          lineNumbers: "off",
          automaticLayout: true,
          fontSize: "12"
        }}
      />
    </div>
  );
}
