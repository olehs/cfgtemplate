import { useConfig } from "../hooks";
import { createValue, hashCode } from "../utils/utils";
import { Editor } from "./Editor";

export function ArrayEditor({ property, path }) {
  const [value, setValue] = useConfig(property, path);
  const values = value || [];

  const items = property.items;

  return (
    <div className={"ArrayEditor" + (property.required && !value ? " RequiredTitle" : "")}>
      <ol>
        {values.map((item, i) => {
          let key = item;
          if (items.type === undefined || items.type === "object") {
            key = item?.[property.key || "id"] || hashCode(item) + i;
          } else if (items.type === "array") {
            key = hashCode(item) + i;
          }

          return (
            <li key={key}>
              <div className="ArrayItem">
                <Editor keyValue="" property={items} path={`${path}[${i}]`} />
                {!property.readonly ? (
                  <button
                    className="DeleteButton"
                    onClick={() => {
                      const arr = [...values];
                      arr.splice(i, 1);
                      setValue(arr);
                    }}
                  >
                    x
                  </button>
                ) : null}
              </div>
            </li>
          );
        })}
      </ol>
      {!property.readonly ? (
        <button
          className="AddButton"
          onClick={() => {
            values.push(undefined);
            setValue(values);
          }}
        >
          Add
        </button>
      ) : null}
    </div>
  );
}
