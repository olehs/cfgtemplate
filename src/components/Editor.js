import { StringEditor } from "./StringEditor";
import { IntEditor } from "./IntEditor";
import { BooleanEditor } from "./BooleanEditor";
import { ArrayEditor } from "./ArrayEditor";
import { ObjectEditor } from "./ObjectEditor";
import { EnumEditor } from "./EnumEditor";
import { useProperty } from "../hooks";
import { TabsEditor } from "./TabsEditor";

export function Editor({ keyValue, property: rawProp, path }) {
  const getProperty = useProperty();

  if (!rawProp) return null;
  const property = getProperty(rawProp);

  const pathName = property.path || keyValue;
  const propertyPath = `${path}${pathName ? "/" + pathName : ""}`;

  if (property.type === "group") {
    return <TabsEditor key={keyValue} property={property} path={propertyPath} />;
  }

  if (property.enum) {
    return <EnumEditor key={keyValue} property={property} path={propertyPath} />;
  }

  switch (property.type) {
    case "string":
      return <StringEditor key={keyValue} property={property} path={propertyPath} />;

    case "integer":
      return <IntEditor key={keyValue} property={property} path={propertyPath} />;

    case "boolean":
      return <BooleanEditor key={keyValue} property={property} path={propertyPath} />;

    case "array":
      return <ArrayEditor key={keyValue} property={property} path={propertyPath} />;

    case undefined:
    case "object":
      return <ObjectEditor key={keyValue} property={property} path={propertyPath} />;

    default:
      console.log("unknown property type " + property.type);
      return null;
  }
}
